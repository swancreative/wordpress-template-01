const gulp = require('gulp');
const sass = require('gulp-sass');
const gutil = require('gulp-util');
const plumber = require('gulp-plumber');
const rename = require('gulp-rename');
const minifycss = require('gulp-minify-css');
const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync');
const cp = require('child_process');

// path variables
const sass_path = './_sass/**/*.scss';
const watch_paths = [
	'./**/*',
	'!./_site/**/*',
	'!./node_modules/**/*',
	'!./_config.yml',
	'!./gulpfile.js',
	'!./package.json'
];

// compile sass
gulp.task('compile-sass', function() {
	return gulp.src(sass_path)
		.pipe(plumber((error) => {
			gutil.log(gutil.colors.red(error.message));
			gulp.task('compile-sass').emit('end');
		}))
		.pipe(sass())
		.pipe(autoprefixer())
		.pipe(minifycss())
		.pipe(rename({
			dirname: './css',
			suffix: '.min'
		}))
		.pipe(gulp.dest('./'))
		.pipe(browserSync.reload({stream: true}));
});

// build jekyll
gulp.task('jekyll-build', function(done) {
	browserSync.notify('Building Jekyll');
	return cp.spawn('jekyll', ['build'], {stdio: 'inherit'})
		.on('error', (error) => gutil.log(gutil.colors.red(error.message)))
		.on('close', done);
});

// reload
gulp.task('jekyll-rebuild', ['jekyll-build'], function() {
	browserSync.reload();
});

// server
gulp.task('browser-sync', ['jekyll-build'], function() {
	browserSync({
		server: {
			baseDir: './_site'
		},
		port: 8888,
		ui: {
			port: 8889
		}
	});
});

// watch
gulp.task('watch', function() {
	gulp.watch(sass_path, ['compile-sass']);
	gulp.watch(watch_paths, ['jekyll-rebuild']);
});

// build
gulp.task('build', ['compile-sass', 'jekyll-build']);

// do everything by default
gulp.task('default', ['compile-sass', 'browser-sync', 'watch']);
